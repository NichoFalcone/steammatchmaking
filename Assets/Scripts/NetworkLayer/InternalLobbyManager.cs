﻿using Steamworks;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MatchZilla
{
    public class InternalLobbyManager : MonoBehaviour
    {
        [SerializeField]
        private LobbyPlayer m_lobbyPlayer;

        [SerializeField]
        private Transform m_playersRoot;

        private List<LobbyPlayer> m_players = new List<LobbyPlayer>();
        private CSteamID m_lobbyId;

        private Callback<LobbyEnter_t> m_LobbyEntered;
        private CallResult<LobbyDataUpdate_t> m_LobbyUpdate;
        private Callback<LobbyChatMsg_t> m_lobbyChatMsg;
        private byte[] m_messageData;

        private void Awake()
        {

        }

        private void Start()
        {
            if (SteamManager.Initialized)
            {
                ///Init all callback for caching data
                m_LobbyEntered = Callback<LobbyEnter_t>.Create(OnLobbyEntered);
                m_LobbyUpdate = CallResult<LobbyDataUpdate_t>.Create(OnLobbyUpdate);
                m_lobbyChatMsg = Callback<LobbyChatMsg_t>.Create(OnRecivingMessage);
            }
            SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, 4);
        }

        private void OnRecivingMessage(LobbyChatMsg_t param)
        {
            Debug.Log("Message");
            switch (SteamHelper.ReadMessage(m_lobbyId, (int)param.m_iChatID, out m_messageData))
            {
                case MessageType.PlayreReady:
                    int playerIndex = m_players.FindIndex((x) => x.SteamID.m_SteamID == param.m_ulSteamIDUser);
                    m_players[playerIndex].Ready = SteamHelper.ReadBool(m_messageData);
                    break;
            }
        }

        private void OnLobbyUpdate(LobbyDataUpdate_t param, bool bIOFailure)
        {
            throw new NotImplementedException();
        }

        private void OnLobbyEntered(LobbyEnter_t param)
        {
            CSteamID lobbyId = new CSteamID(param.m_ulSteamIDLobby);
            m_lobbyId = lobbyId;
            int usersCount = SteamMatchmaking.GetNumLobbyMembers(lobbyId);

            for (int i = 0; i < usersCount; i++)
            {
                CSteamID user = SteamMatchmaking.GetLobbyMemberByIndex(lobbyId, i);
                if (m_players.Find((x) => x.SteamID == user))
                    continue;
                else
                {
                    LobbyPlayer player = Instantiate(m_lobbyPlayer, m_playersRoot);
                    player.SteamID = user;
                    SteamNetworkManager.Instance.LobbyID = m_lobbyId;
                    m_players.Add(player);
                }
            }
        }
    }
}