﻿using Steamworks;
using UnityEngine;

namespace MatchZilla
{
    [System.Serializable]
    public struct SteamPlayerInfo
    {
        private CSteamID m_steamID;
        [SerializeField]
        private Texture2D m_steamImage;

        private string m_steamName;

        public CSteamID SteamID {
            get => m_steamID;
        }
        public Texture2D SteamImage {
            get => m_steamImage;
        }

        public string SteamName {
            get => m_steamName;
        }

        public SteamPlayerInfo(CSteamID m_steamID, Texture2D m_image, string m_name)
        {
            this.m_steamID      = m_steamID;
            this.m_steamImage   = m_image;
            this.m_steamName    = m_name;
        }
    }
}