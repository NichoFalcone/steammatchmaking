﻿using UnityEngine;
using UnityEngine.Networking;
using Steamworks;

public class NetworkPlayerMovement : NetworkBehaviour
{
    private bool m_bCanMove = true;
    private CameraFollower m_cameraFollower;
    private Rigidbody m_rb;
    [SerializeField]
    private float m_accelerationSpeed = 100;
    [SerializeField]
    private float m_maxSpeed = 200;
    [SerializeField]
    private float m_accelerationSteering = 20;
    [SerializeField]
    private ParticleSystem[] m_smoke;
    private bool isHost {
        get => isServer && isClient;
    }

    private void Awake()
    {
        m_cameraFollower = FindObjectOfType<CameraFollower>();
        m_rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        if (!m_cameraFollower)
        {
            m_bCanMove = false;
            m_rb.useGravity = false;
        }
        else
        {
            m_bCanMove = true;
            m_rb.useGravity = true;
        }
    }

    private void Update()
    {
        if (!m_bCanMove || !hasAuthority)
            return;
        float throttle = Input.GetAxis("Vertical");
        float _steering = Input.GetAxis("Horizontal");

        if (Input.GetKey(KeyCode.Space))
        {
            m_rb.AddForce( new Vector3(-m_rb.velocity.x, 0 ,-m_rb.velocity.z));
        }

        Vector3 force = transform.forward * m_accelerationSpeed * throttle;
        m_rb.AddForce(force, ForceMode.Acceleration);
        m_rb.velocity = Vector3.ClampMagnitude(m_rb.velocity, m_maxSpeed);

        float driftValue = Vector3.Dot(m_rb.velocity.normalized, transform.right);
        float driftAngle = Mathf.Asin(driftValue) * Mathf.Rad2Deg;

        for (int i = 0; i < m_smoke.Length; i++)
        {
            if (Mathf.Abs(driftAngle) > 15 && !m_smoke[i].isPlaying)
            {
                m_smoke[i].Play();
            }
            else if(Mathf.Abs(driftAngle) < 15)
            {
                m_smoke[i].Stop();
            }
        }

        transform.Rotate(Vector3.up * _steering);
    }
}
