﻿using Steamworks;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.Networking.NetworkSystem;

namespace MatchZilla
{
    [System.Serializable]
    public class UNETServer
    {
        #region Private-Field
        /// <summary>
        /// Reference of the connection info and data
        /// </summary>
        [SerializeField]
        private ConnectionData m_connectionData = new ConnectionData();
        /// <summary>
        /// Player prefab
        /// </summary>
        [SerializeField]
        private GameObject m_playerPrefab = null;
        private List<NetworkConnection> m_connectedClients = new List<NetworkConnection>();
        /// Steamworks callbacks
        private Callback<P2PSessionRequest_t> m_P2PSessionRequested;
        #endregion

        #region Public-Field
        /// local client-to-server connection
        public NetworkClient Client {
            get {
                return SteamNetworkManager.Instance.NetworkClient;
            }
            set {
                SteamNetworkManager.Instance.NetworkClient = value;
            }
        }
        /// <summary>
        /// Return if the server is active
        /// </summary>
        public bool IsHostingServer => NetworkServer.active;
        public ConnectionData ConnectionData => m_connectionData;
        public List<NetworkConnection> Connections => m_connectedClients;

        #endregion

        /// <summary>
        /// Init the list of connected client and
        /// start listenind to the callback
        /// </summary>
        internal void Init()
        {
            m_connectedClients = new List<NetworkConnection>();
            if (SteamManager.Initialized)
            {
                ///Create and start listening the callback to the new session required
                m_P2PSessionRequested = Callback<P2PSessionRequest_t>.Create(OnP2PSessionRequested);
            }
        }
        /// <summary>
        /// Start the local server 
        /// </summary>
        internal void StartUNETServer()
        {
            if (SteamNetworkManager.Instance.ConnectionState != ConnectionState.CONNECTED)
            {
                Debug.LogError("[UNET Server] Not connected to lobby");
                return;
            }

            Debug.Log("[UNET Server] Starting UNET server");

            // Listen for player spawn request messages 
            NetworkServer.RegisterHandler(NetworkMessages.SpawnRequestMsg, OnSpawnRequested);

            // Start UNET server
            NetworkServer.Configure(ConnectionData.HostTopology);
            NetworkServer.dontListen = true;
            NetworkServer.Listen(0);

            // Create a local client-to-server connection to the "server"
            // Connect to localhost to trick UNET's ConnectState state to "Connected", which allows data to pass through TransportSend
            Client = ClientScene.ConnectLocalServer();
            Client.Configure(ConnectionData.HostTopology);
            Client.Connect(ConnectionData.ServerAddress, ConnectionData.ServerPort);
            Client.connection.ForceInitialize();

            // Add local client to server's list of connections
            // Here we get the connection from the NetworkServer because it represents the server-to-client connection
            NetworkConnection serverToClientConn = NetworkServer.connections[0];
            AddConnection(serverToClientConn);

            // register networked prefabs
            SteamNetworkManager.Instance.RegisterNetworkPrefabs();
            // Spawn self
            ClientScene.Ready(serverToClientConn);
        }
        /// <summary>
        /// Spawn a player
        /// </summary>
        /// <param name="_conn">network connection</param>
        /// <returns></returns>
        public bool SpawnPlayer(NetworkConnection _conn)
        {
            NetworkServer.SetClientReady(_conn);
            GameObject player = GameObject.Instantiate(m_playerPrefab);
            Debug.Log("[UNET server] player Spawned");
            return NetworkServer.SpawnWithClientAuthority(player, _conn);
        }
        /// <summary>
        /// Spawn a player with a specific position and rotation
        /// </summary>
        /// <param name="_conn">player connection</param>
        /// <param name="_startPosition">disired position and orientation</param>
        /// <returns></returns>
        public bool SpawnPlayer(NetworkConnection _conn, Transform _startPosition)
        {
            NetworkServer.SetClientReady(_conn);
            GameObject player = GameObject.Instantiate(m_playerPrefab, _startPosition.position, _startPosition.rotation);
            Debug.Log("[UNET server] player Spawned");
            return NetworkServer.SpawnWithClientAuthority(player, _conn);
        }
        /// <summary>
        /// Search the players connected and destroy the selected by the server side
        /// </summary>
        /// <param name="_conn"></param>
        private void DestroyPlayer(NetworkConnection _conn)
        {
            if (_conn == null)
                return;
            // quick and dirty hack to destroy a player. GameObject.FindObjectsOfType probably shouldn't be used here.
            var objs = GameObject.FindObjectsOfType<NetworkIdentity>();
            for (int i = 0; i < objs.Length; i++)
            {
                ///Check the autority and if the connection id is matching
                if (objs[i].clientAuthorityOwner != null && objs[i].clientAuthorityOwner.connectionId == _conn.connectionId)
                {
                    ///Destroy
                    NetworkServer.Destroy(objs[i].gameObject);
                }
            }
        }
        /// <summary>
        /// Add the current connection on the connected client list
        /// </summary>
        /// <param name="_conn"></param>
        public void AddConnection(NetworkConnection _conn)
        {
            Debug.Log("[UNET Server] Connecting client from: " + _conn.address);
            m_connectedClients.Add(_conn);
        }
        /// <summary>
        /// Remove a NetworkConnection
        /// </summary>
        /// <param name="_steamID">target steadID</param>
        internal void RemoveConnection(CSteamID _steamID)
        {
            NetworkConnection conn = GetClient(_steamID);
            SteamNetworkConnection steamConn = conn as SteamNetworkConnection;
            if (conn != null)
            {
                if (steamConn != null)
                {
                    steamConn.CloseP2PSession();
                }

                DestroyPlayer(conn);
                m_connectedClients.Remove(conn);

                conn.hostId = -1;
                conn.Disconnect();
                conn.Dispose();
                conn = null;
            }
        }
        /// <summary>
        /// Get the total open channel on the server
        /// </summary>
        internal int GetChannelCount()
        {
            return ConnectionData.HostTopology.DefaultConfig.Channels.Count;
        }
        /// <summary>
        /// Request a new session
        /// </summary>
        /// <param name="pCallback"></param>
        private void OnP2PSessionRequested(P2PSessionRequest_t pCallback)
        {
            Debug.Log("[UNET Server] P2P session request received");
            ///Get the steam id fron the callback session
            CSteamID member = pCallback.m_steamIDRemote;
            ///Check if the user is in the join on the steamlobby
            if (NetworkServer.active && SteamNetworkManager.Instance.IsMemberInLobby(member))
            {
                // Accept the connection if this user is in the lobby
                Debug.Log("[UNET Server] P2P connection accepted");
                SteamNetworking.AcceptP2PSessionWithUser(member);
                ///Create a new connection with the new peer
                CreateP2PConnectionWithPeer(member);
            }
        }
        /// <summary>
        /// Disconnect all peers and shutdown the server
        /// </summary>
        internal void Disconnect()
        {
            for (int i = m_connectedClients.Count - 1; i >= 0; i--)
            {
                NetworkServer.SetClientNotReady(m_connectedClients[i]);
                SteamNetworkConnection steamConn = m_connectedClients[i] as SteamNetworkConnection;
                if (steamConn != null)
                {
                    if (SteamManager.Initialized)
                    {
                        RemoveConnection(steamConn.steamId);
                        Debug.Log("[UNET Server] Removing connection");
                        SteamNetworking.CloseP2PSessionWithUser(steamConn.steamId);
                    }
                }
            }
            ///Clear connection list
            m_connectedClients.Clear();

            if (NetworkServer.active)
            {
                ///Shot down the server and clear all data
                NetworkServer.Shutdown();
            }
        }
        /// <summary>
        /// Read the contents of this message. It should contain the steam ID of the sender
        /// and spawn the player on scene
        /// </summary>
        /// <param name="msg"></param>
        private void OnSpawnRequested(NetworkMessage msg)
        {
            Debug.Log("[UNET Server] Spawn request received");
            StringMessage strMsg = msg.ReadMessage<StringMessage>();
            if (strMsg != null)
            {
                ///reference to the steamid recived from the message
                ///send from the client
                ulong steamId;
                if (ulong.TryParse(strMsg.value, out steamId))
                {
                    var conn = GetClient(new CSteamID(steamId));

                    if (conn != null)
                    {
                        // spawn peer
                        if (SpawnPlayer(conn))
                        {
                            return;
                        }
                    }
                }
            }
            Debug.LogError("[UNET Server] Failed to spawn player");
        }
        /// <summary>
        /// Start searching throw the connection to find a NetworkConnection with the same CSteamID
        /// </summary>
        /// <param name="steamId"></param>
        /// <returns></returns>
        public NetworkConnection GetClient(CSteamID steamId)
        {
            if (steamId.m_SteamID == SteamUser.GetSteamID().m_SteamID)
            {
                // get the local client
                if (NetworkServer.active && NetworkServer.connections.Count > 0)
                {
                    return NetworkServer.connections[0];
                }
            }

            // find remote client
            for (int i = 0; i < m_connectedClients.Count; i++)
            {
                SteamNetworkConnection steamConn = m_connectedClients[i] as SteamNetworkConnection;
                if (steamConn != null && steamConn.steamId.m_SteamID == steamId.m_SteamID)
                {
                    return steamConn;
                }
            }

            Debug.Log("[UNET Server] Client not found");
            return null;
        }
        /// <summary>
        /// sening back a packet to the connection pending to the server
        /// </summary>
        /// <param name="peer"></param>
        public void CreateP2PConnectionWithPeer(CSteamID peer)
        {
            Debug.Log("[UNET Server] Sending P2P acceptance message and creating remote client reference for UNET server");
            SteamNetworking.SendP2PPacket(peer, null, 0, EP2PSend.k_EP2PSendReliable);

            /// create new connnection for this client and connect them to server
            SteamNetworkConnection newConn = new SteamNetworkConnection(peer);
            /// forece the initizlization adding the a new connection id
            newConn.ForceInitialize();

            NetworkServer.AddExternalConnection(newConn);
            AddConnection(newConn);
        }

    }
}