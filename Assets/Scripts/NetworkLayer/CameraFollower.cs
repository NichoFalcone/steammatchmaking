﻿using UnityEngine;
public class CameraFollower : MonoBehaviour
{
    public Transform target;
    public float height = 3.0f;
    public float damping = .0f;
    private float distance = 5f;
    private Vector3 m_nextPosition;
    public Transform Target {

        get => target;
        set => target = value;
    }

    void FixedUpdate()
    {
        if (target == null)
            return;

        Vector3 wantedPosition = target.TransformPoint(0, height, -distance);
        m_nextPosition = Vector3.Lerp(transform.position, wantedPosition, Time.fixedDeltaTime * damping);
        transform.LookAt(target);
    }
    private void LateUpdate()
    {
        transform.position = m_nextPosition;
    }
}