﻿using UnityEngine;
using UnityEngine.Networking;

namespace MatchZilla
{
    [System.Serializable]
    public struct ConnectionData
    {
        /// <summary>
        /// The server address
        /// </summary>
        [SerializeField]
        private string m_address;
        /// <summary>
        /// The server port to keep listend
        /// </summary>
        [SerializeField]
        private int m_port;

        /// <summary>
        /// The current hostTopology of this network
        /// by default generated on the propery
        /// </summary>
        private HostTopology m_hostTopology;

        /// <summary>
        /// Server address on this case on the p2p our localaddress
        /// </summary>
        public string ServerAddress {
            get { return m_address; }
        }

        /// <summary>
        /// Server port by default 0
        /// </summary>
        public int ServerPort {
            get { return m_port; }
        }

        /// <summary>
        /// HostTopology genereted with the following parameters:
        /// 2 channel -1 ReliableSequenced 2-Unreliable
        /// max connection taked from the LobbyData
        /// </summary>
        public HostTopology HostTopology {
            get {
                if (m_hostTopology == null)
                {
                    ///Create a configuration with 2 channel reliableSqeunced and unreliable
                    ConnectionConfig config = new ConnectionConfig();
                    ///Garanted to deliver a message with order
                    config.AddChannel(QosType.ReliableSequenced);
                    ///There is no guarantee of delivery or ordering.
                    config.AddChannel(QosType.Unreliable);
                    ///Create a new configuration and get the max connection number
                    m_hostTopology = new HostTopology(config, SteamNetworkManager.Instance.LobbyData.GameMode.MaxLobbyPlayer);
                }
                return m_hostTopology;
            }
        }
    }
}