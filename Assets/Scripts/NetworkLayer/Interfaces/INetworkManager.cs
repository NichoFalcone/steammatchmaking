﻿using System;
using UnityEngine.Networking;

namespace MatchZilla
{
    /// <summary>
    /// Interface used to create a base structure to implement the same system on multiple scenarios
    /// </summary>
    /// <typeparam name="T">The API structure to identity the lobby data and user data</typeparam>
    /// <typeparam name="K">the Type to identity the RoomData</typeparam>
    public interface INetworkManager<T, K>
    {
        #region Propery
        /// <summary>
        /// Lobby reference
        /// </summary>
        ILobbyData LobbyData {
            get;
        }

        /// <summary>
        /// Client-to-Server connection
        /// </summary>
        NetworkClient NetworkClient {
            get;
        }

        /// <summary>
        /// Server Network Reference
        /// </summary>
        UNETServer Server {
            get;
        }

        /// <summary>
        /// Current connetion state of the server
        /// </summary>
        ConnectionState ConnectionState {
            get;
        }

        /// <summary>
        /// Call this action to send the data of the room
        /// each time you need to notifie the change
        /// </summary>
        Action<K> OnUserChange {
            get; set;
        }
        #endregion

        #region Method
        /// <summary>
        /// use this function to starting a lobby to join
        /// if the searching has a negative resoult, start a new one lobby
        /// and open a new connection
        /// </summary>
        void FindMatch();

        /// <summary>
        /// Use this function to join on the server
        /// and start another one and start sincronize the connection
        /// </summary>
        void Connect();

        /// <summary>
        /// Use this function for kill the server and leave the steam lobby
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Join on a lobby
        /// </summary>
        /// <param name="_data"></param>
        void JoinLobby(T _data);

        /// <summary>
        /// Check if the current data player is on the lobby
        /// </summary>
        /// <param name="_data"></param>
        bool IsMemberInLobby(T _data);

        /// <summary>
        /// Check if the current data player is the owenr of the lobby
        /// </summary>
        /// <param name="_data"></param>
        /// <returns></returns>
        bool CheckOwner(T _data);

        /// <summary>
        /// Use the current data to throw a new network connection on the server
        /// </summary>
        /// <param name="_data"></param>
        void ConnectToUnetServer(T _data);
        /// <summary>
        /// This function spawn a new player on the scene
        /// </summary>
        void SpawnPlayers();

        /// <summary>
        /// Use this function to change the state of the lobby.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="_nextState"></param>
        void ChangeLobbyState(LobbyState _nextState);
        #endregion
    }
}

