﻿using Steamworks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MatchZilla
{
    public class SteamNetworkManager : Singleton<SteamNetworkManager>, INetworkManager<CSteamID, SteamRoomData>
    {
        #region Private-Field
        /// <summary>
        /// Reference to the server
        /// </summary>
        [SerializeField]
        private UNETServer m_server                         = null;
        /// <summary>
        /// Reference to the lobbyData
        /// to send on the LobbyManager when it connect
        /// </summary>
        [SerializeField]
        private LobbyData m_lobbyData                       = null;
        /// <summary>
        /// The current state of the connection to the server
        /// </summary>
        [SerializeField]
        private ConnectionState m_connectionState           = ConnectionState.DISCONNECTED;
        /// <summary>
        /// Client-to-Server connection
        /// </summary>
        private NetworkClient m_networkClient               = null;

        /// <summary>
        /// Action startd from the UpdateLobby and implemented on LobbyManager
        /// Sending the structure of the SteamRoomData
        /// </summary>
        private Action<SteamRoomData> m_onUserChangeHandler = null;
        /// <summary>
        /// The reference of the registered prefabs
        /// to add on the network server
        /// </summary>
        [SerializeField]
        private List<GameObject> m_networkPrefabs           = new List<GameObject>();

        private byte[] m_messageData;

        #region Steam-Field
        /// <summary>
        /// the steam id of the lobby where we go to create or connect
        /// </summary>
        private CSteamID m_lobbyID;
        /// <summary>
        /// The steam id of the owner of the Lobby
        /// </summary>
        private CSteamID m_lobbyOwner;
        /// <summary>
        /// Cache for the output of the packets size
        /// used on the update for the communication P2P
        /// </summary>
        uint m_packetSize;
        /// <summary>
        /// 
        /// </summary>
        private Callback<LobbyEnter_t> m_LobbyEntered;
        /// <summary>
        /// 
        /// </summary>
        private Callback<GameLobbyJoinRequested_t> m_GameLobbyJoinRequested;
        /// <summary>
        /// 
        /// </summary>
        private Callback<LobbyChatUpdate_t> m_LobbyChatUpdate;
        /// <summary>
        /// 
        /// </summary>
        private Callback<LobbyChatMsg_t> m_lobbyChatMsg;
        /// <summary>
        /// Callback used when we request a list of Lobby
        /// </summary>
        private CallResult<LobbyMatchList_t> m_LobbyMatchList;
        /// <summary>
        /// Callback used when the lobby is updating the data
        /// </summary>
        private CallResult<LobbyDataUpdate_t> m_LobbyUpdate;
        #endregion
        #endregion

        #region Public-Field
        /// <summary>
        /// Lobby data reference for the max connection
        /// </summary>
        public ILobbyData LobbyData => m_lobbyData;
        /// <summary>
        /// The reference of the server
        /// </summary>
        public UNETServer Server => m_server;

        public ConnectionState ConnectionState {
            get => m_connectionState;
            set 
            {
                m_connectionState = value;
                if (OnConnectionStateChange != null)
                    OnConnectionStateChange(value);
            }
        }
        /// <summary>
        /// Client-to-Server connection
        /// </summary>
        public NetworkClient NetworkClient { get => m_networkClient; set => m_networkClient = value; }
        public CSteamID LobbyID { get => m_lobbyID; set => m_lobbyID = value; }
        public Action<SteamRoomData> OnUserChange { get => m_onUserChangeHandler; set => m_onUserChangeHandler = value; }

        /// <summary>
        /// Event to notifie the recived message with the new value of the timer
        /// </summary>
        /// <param name="_currentTime"></param>
        public delegate void ReciveTimeHandler(int _currentTime);
        public event ReciveTimeHandler OnReciveTime;

        public delegate void LobbyStateChangeHandler(bool _staate);
        public event LobbyStateChangeHandler OnLobbyStateChange;

        public delegate void ConnectionStateChangeHandler(ConnectionState _currentState);
        public event ConnectionStateChangeHandler OnConnectionStateChange;
        #endregion

        #region Unity-Method
        protected override void Awake()
        {
            ///execute the base mehtod for the singleton instance
            base.Awake();
            /// Set the applcitation to run in background
            Application.runInBackground = true;
        }

        private void Start()
        {
            ///Setup log file
            LogFilter.currentLogLevel = LogFilter.Info;

            if (SteamManager.Initialized)
            {
                ///Init all callback for caching data
                m_LobbyEntered              = Callback<LobbyEnter_t>.Create(OnLobbyEntered);
                m_GameLobbyJoinRequested    = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyJoinRequested);
                m_LobbyChatUpdate           = Callback<LobbyChatUpdate_t>.Create(OnLobbyChatUpdate);
                m_LobbyMatchList            = CallResult<LobbyMatchList_t>.Create(OnLobbyMatchList);
                m_LobbyUpdate               = CallResult<LobbyDataUpdate_t>.Create(OnLobbyUpdate);
                m_lobbyChatMsg              = Callback<LobbyChatMsg_t>.Create(OnRecivingMessage);
            }
            else
            {
                ConnectionState = ConnectionState.FAILED;
            }
            ///init the server data
            m_server.Init();
        }

        private void Update()
        {
            ///Check the SteamManager initialization
            if (!SteamManager.Initialized)
            {
                return;
            }
            ///Check the connection on the server
            if (!IsConnectedToUNETServer())
            {
                return;
            }
            /// Read Steam packets
            /// Get the QOC channel count
            for (int _chan = 0; _chan < Server.GetChannelCount(); _chan++)
            {
                /// Check if are package available
                while (SteamNetworking.IsP2PPacketAvailable(out m_packetSize, _chan))
                {
                    ///Set byte size
                    byte[] data = new byte[m_packetSize];
                    ///Create a senderid
                    CSteamID _senderId;
                    ///Read package
                    if (SteamNetworking.ReadP2PPacket(data, m_packetSize, out m_packetSize, out _senderId, _chan))
                    {
                        ///Create a new connection 
                        NetworkConnection _conn;

                        if (Server.IsHostingServer)
                        {
                            // We are the server, one of our clients will handle this packet
                            _conn = Server.GetClient(_senderId);

                            if (_conn == null)
                            {
                                // In some cases the p2p connection can persist, resulting in UNETServerController.OnP2PSessionRequested not being called. This happens usually when testing in editor.
                                // If the peers have already established a connection, reset it.
                                P2PSessionState_t _sessionState;
                                if (SteamNetworking.GetP2PSessionState(_senderId, out _sessionState) && Convert.ToBoolean(_sessionState.m_bConnectionActive))
                                {
                                    Debug.Log("[SteamNetworkManager] P2P connection is still established. Resetting.");
                                    SteamNetworking.CloseP2PSessionWithUser(_senderId);
                                    Server.CreateP2PConnectionWithPeer(_senderId);
                                    _conn = Server.GetClient(_senderId);
                                }
                            }
                        }
                        else
                        {
                            /// We are a client, we only have one connection (the server).
                            _conn = m_networkClient.connection;
                        }

                        if (_conn != null)
                        {
                            /// Handle Steam packet through UNET
                            _conn.TransportReceive(data, Convert.ToInt32(m_packetSize), _chan);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Send to all game objects before the applicaition quit
        /// </summary>
        private void OnApplicationQuit()
        {
            Debug.Log("[SteamNetworkManager] Application suspend, killing network connections");
            ///Forzed the disconnection
            Disconnect();
        }

        #endregion

        #region Public-Method

        /// <summary>
        /// in this case is not used...
        /// but the idea is to used this function to create 
        /// a direct connection to a lobby selcted from UI
        /// </summary>
        public void Connect()
        {
            ///...
        }

        /// <summary>
        /// Disconnect the current user from the server and from the SteamAPI side
        /// and clear all cache
        /// </summary>
        public void Disconnect()
        {
            if (ConnectionState == ConnectionState.DISCONNECTED)
                return;

            ConnectionState = ConnectionState.DISCONNECTED;
            ClientScene.DestroyAllClientObjects();

            if (SteamManager.Initialized)
            {
                SteamMatchmaking.LeaveLobby(m_lobbyID);
            }

            if (m_networkClient != null)
            {
                m_networkClient.Disconnect();
                m_networkClient = null;
            }
            Debug.Log("[SteamNetworkManager] Disconnecting");
            m_server.Disconnect();
            NetworkClient.ShutdownAll();
            m_lobbyID.Clear();
        }
        
        /// <summary>
        /// Start searching a Game with the same filter
        /// </summary>
        public void FindMatch()
        {
            if (!SteamManager.Initialized)
            {
                ConnectionState = ConnectionState.FAILED;
                return;
            }
            ConnectionState = ConnectionState.CONNECTING;


            Debug.Log("[SteamNetworkManager] Starting serching for game of type | " + LobbyData.GameMode.GameModeName);
            //Note: call SteamMatchmaking.AddRequestLobbyList* before RequestLobbyList to filter results by some criteria
            SteamMatchmaking.AddRequestLobbyListStringFilter(LobbyData.GameMode.GameModeName, m_lobbyData.RoomName, ELobbyComparison.k_ELobbyComparisonEqual);
            SteamAPICall_t _callBack = SteamMatchmaking.RequestLobbyList();
            m_LobbyMatchList.Set(_callBack, OnLobbyMatchList);
        }
        #endregion

        #region SteamInteraction
        /// <summary>
        /// Callback started from the steamAPI 
        /// called from the arrived message on the lobby        
        /// </summary>
        /// <param name="param"></param>
        private void OnRecivingMessage(LobbyChatMsg_t param)
        {
            switch (SteamHelper.ReadMessage(m_lobbyID, (int)param.m_iChatID, out m_messageData))
            {
                case MessageType.Timer:
                    int time = SteamHelper.ReadInt(m_messageData);
                    Debug.Log("timer update " + time);
                    if (OnReciveTime != null)
                        OnReciveTime(time);
                    break;

                case MessageType.PlayerJoined:
                    Debug.Log("Player joined");
                    if (OnLobbyStateChange != null)
                        OnLobbyStateChange(SteamHelper.ReadBool(m_messageData));
                    break;

                case MessageType.PlayreReady:
                    Debug.Log("asdadawda");
                    break;

                case MessageType.PlayerCount:
                    Debug.Log(SteamHelper.ReadString(m_messageData));
                    break;
            }
        }

        /// <summary>
        /// Callback started from the steamAPI 
        /// called from the joining feedback
        /// </summary>
        /// <param name="_pCallback">lobby info</param>
        private void OnLobbyEntered(LobbyEnter_t _pCallback)
        {
            if (m_connectionState != ConnectionState.CONNECTING)
                return;
            if (!SteamManager.Initialized)
            {
                ConnectionState = ConnectionState.FAILED;
                return;
            }

            ///Create a new session ID
            m_lobbyID = new CSteamID(_pCallback.m_ulSteamIDLobby);
            ///Set the season state
            ConnectionState = ConnectionState.CONNECTED;
            ///Get the owner
            m_lobbyOwner = SteamMatchmaking.GetLobbyOwner(m_lobbyID);
            ///Get my steam ID
            CSteamID me = SteamUser.GetSteamID();
            ///Check if i'm the owner
            if (m_lobbyOwner.m_SteamID == me.m_SteamID)
            {
                ///Start the UnetServer
                m_server.StartUNETServer();
                ///Set the new data of the lobby
                SteamMatchmaking.SetLobbyData(m_lobbyID, LobbyData.GameMode.GameModeName, m_lobbyData.RoomName);
                SteamAPICall_t _callBack = SteamMatchmaking.RequestLobbyList();
                m_LobbyUpdate.Set(_callBack, OnLobbyUpdate);
            }
            else
            {
                ///Start the connection the the founded lobby
                StartCoroutine(RequestP2PConnectionWithHost());
                ///Set the new data of the lobby
                SteamAPICall_t _callBack = SteamMatchmaking.RequestLobbyList();
                m_LobbyUpdate.Set(_callBack, OnLobbyUpdate);
                SteamHelper.SendMessage(m_lobbyID, MessageType.PlayerJoined ,System.BitConverter.GetBytes(false));
            }
        }
        /// <summary>
        /// Called each time the lobby is updated
        /// </summary>
        /// <param name="_param"></param>
        /// <param name="_bIOFailure"></param>
        private void OnLobbyUpdate(LobbyDataUpdate_t _param, bool _bIOFailure)
        {
            if (m_lobbyID != null)
            {
                List<CSteamID> _users = new List<CSteamID>();
                for (int i = 0; i < SteamMatchmaking.GetNumLobbyMembers(m_lobbyID); i++)
                {
                    _users.Add(SteamMatchmaking.GetLobbyMemberByIndex(m_lobbyID, i));
                }
                SteamRoomData _currentRoomData = new SteamRoomData(m_lobbyData, _users, m_lobbyOwner, m_lobbyID);
                ///Create and send the connection and lobby info to the lobby manager 
                if (OnUserChange != null)
                    OnUserChange.Invoke(_currentRoomData);
            }
        }

        /// <summary>
        /// Start a the requestion connection to the host and wait for a feed back from it 
        /// </summary>
        /// <returns></returns>
        private IEnumerator RequestP2PConnectionWithHost()
        {
            Debug.Log("[SteamNetworkManager] Sending packet to request P2P connection");
            ///send packet to request connection to host via Steam's NAT punch or relay servers
            SteamNetworking.SendP2PPacket(m_lobbyOwner, null, 0, EP2PSend.k_EP2PSendReliable);

            Debug.Log("[SteamNetworkManager] Waiting for P2P acceptance message");
            uint packetSize;
            while (!SteamNetworking.IsP2PPacketAvailable(out packetSize))
            {
                yield return null;
            }

            byte[] data = new byte[packetSize];

            CSteamID _senderId;

            if (SteamNetworking.ReadP2PPacket(data, packetSize, out packetSize, out _senderId))
            {
                if (_senderId.m_SteamID == m_lobbyOwner.m_SteamID)
                {
                    Debug.Log("[SteamNetworkManager] P2P connection established");

                    /// packet was from host, assume it's notifying client that AcceptP2PSessionWithUser was called
                    P2PSessionState_t _sessionState;
                    if (SteamNetworking.GetP2PSessionState(m_lobbyOwner, out _sessionState))
                    {
                        /// connect to the unet server
                        ConnectToUnetServer(m_lobbyOwner);
                        yield break;
                    }
                }
            }
            Debug.LogError("[SteamNetworkManager] Connection failed");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_pCallback"></param>
        private void OnGameLobbyJoinRequested(GameLobbyJoinRequested_t _pCallback)
        {
            // Invite accepted, game is already running
            JoinLobby(_pCallback.m_steamIDLobby);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_lobbyId"></param>
        public void JoinLobby(CSteamID _lobbyId)
        {
            if (!SteamManager.Initialized)
            {
                ConnectionState = ConnectionState.FAILED;
                return;
            }
            ConnectionState = ConnectionState.CONNECTING;
            SteamMatchmaking.JoinLobby(_lobbyId);
            Debug.Log("[SteamNetworkManager] Player joining");
            // ...continued in OnLobbyEntered callback
        }

        /// <summary>
        /// Called each lobby update
        /// On this function, we go to check if the leaved user is the host 
        /// and update the network connection
        /// </summary>
        /// <param name="_pCallback">callback startd from the steam API</param>
        private void OnLobbyChatUpdate(LobbyChatUpdate_t _pCallback)
        {
            ///Set the new data of the lobby
            SteamAPICall_t _callBack = SteamMatchmaking.RequestLobbyList();
            m_LobbyUpdate.Set(_callBack, OnLobbyUpdate);

            if (_pCallback.m_rgfChatMemberStateChange == (uint)EChatMemberStateChange.k_EChatMemberStateChangeLeft && _pCallback.m_ulSteamIDLobby == m_lobbyID.m_SteamID)
            {
                Debug.Log("[SteamNetworkManager] A client has disconnected from the UNET server");

                /// user left lobby
                CSteamID _leftedUser = new CSteamID(_pCallback.m_ulSteamIDUserChanged);
                ///if was the owner disconnect all the current user will disconnect
                if (CheckOwner(_leftedUser))
                {
                    Debug.Log("[SteamNetworkManager] Disconnecting from Server");
                    Disconnect();
                    SceneController.Instance.LoadLobbyScene();
                }

                ///Update on all clinet and 
                ///if is the owner remove the connection from the server
                if (m_server.IsHostingServer)
                {
                    m_server.RemoveConnection(_leftedUser);
                }
                ///Close the P2P connection
                SteamNetworking.CloseP2PSessionWithUser(_leftedUser);
            }
        }
        
        /// <summary>
        /// Check the owner of the current lobby
        /// </summary>
        /// <param name="_data"></param>
        /// <returns></returns>
        public bool CheckOwner(CSteamID _data)
        {
            return _data.m_SteamID == m_lobbyOwner.m_SteamID;
        }

        /// <summary>
        /// On this function we get a list of mathed lobby Called on FindMatch*
        /// if the list count is > of 0 start a connection with the first lobby
        /// else create on with the same parameters
        /// </summary>
        /// <param name="pCallback"></param>
        /// <param name="bIOFailure"></param>
        private void OnLobbyMatchList(LobbyMatchList_t pCallback, bool bIOFailure)
        {
            uint numLobbies = pCallback.m_nLobbiesMatching;
            Debug.Log("[SteamNetworkManager] Founded lobby " + numLobbies);
            if (numLobbies <= 0)
            {
                // no lobbies found. create one
                SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypePublic, LobbyData.GameMode.MaxLobbyPlayer);
                // ...continued in OnLobbyEntered callback
            }
            else
            {
                // If multiple lobbies are returned we can iterate over them with SteamMatchmaking.GetLobbyByIndex and choose the "best" one
                // In this case we are just joining the first one
                CSteamID lobby = SteamMatchmaking.GetLobbyByIndex(0);
                JoinLobby(lobby);
            }
        }
        #endregion

        #region NetworkInteraction
        /// <summary>
        /// Check if my client is connected to the UNETServer
        /// </summary>
        /// <returns>true if is connected</returns>
        public bool IsConnectedToUNETServer()
        {
            return m_networkClient != null && m_networkClient.connection != null && m_networkClient.connection.isConnected;
        }

        /// <summary>
        /// Update the Network prefabs
        /// </summary>
        public void RegisterNetworkPrefabs()
        {
            foreach (GameObject _go in m_networkPrefabs)
            {
                ClientScene.RegisterPrefab(_go);
            }
        }

        /// <summary>
        /// Check if the current user is on the lobby
        /// </summary>
        /// <param name="_data"></param>
        /// <returns></returns>
        public bool IsMemberInLobby(CSteamID _data)
        {
            if (SteamManager.Initialized)
            {
                int numMembers = SteamMatchmaking.GetNumLobbyMembers(m_lobbyID);

                for (int i = 0; i < numMembers; i++)
                {
                    CSteamID member = SteamMatchmaking.GetLobbyMemberByIndex(m_lobbyID, i);

                    if (member.m_SteamID == _data.m_SteamID)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// This function call the server and spawn new player
        /// from the connection extablished on the server
        /// </summary>
        public void SpawnPlayers()
        {
            Debug.Log("[SteamNetworkManager] number of connection = " + m_server.Connections.Count);
            foreach (NetworkConnection connection in m_server.Connections)
            {
                m_server.SpawnPlayer(connection);
            }
        }
        /// <summary>
        /// This function call the server and spawn new player
        /// from the connection extablished on the server
        /// </summary>
        public void SpawnPlayer(NetworkConnection conn, Transform _startPosition)
        {
            m_server.SpawnPlayer(conn, _startPosition);
        }
        /// <summary>
        /// Connect the current user data on the server
        /// </summary>
        /// <param name="_data"></param>
        public void ConnectToUnetServer(CSteamID _data)
        {
            Debug.Log("[SteamNetworkManager] Connecting to UNET server");
            // Create connection to host player's steam ID
            SteamNetworkConnection conn = new SteamNetworkConnection(_data);
            SteamClient mySteamClient = new SteamClient(conn);
            this.m_networkClient = mySteamClient;

            /// Setup and connect
            /// Registering a new handler type connect on the current Client
            mySteamClient.RegisterHandler(MsgType.Connect, OnConnect);
            mySteamClient.SetNetworkConnectionClass<SteamNetworkConnection>();
            /// Setup the Connection type
            mySteamClient.Configure(Server.ConnectionData.HostTopology);
            /// Start the connection
            mySteamClient.Connect();
        }

        /// <summary>
        /// On this function we go tho send the spawn message to the current player
        /// </summary>
        /// <param name="_msg"></param>
        private void OnConnect(NetworkMessage _msg)
        {
            // Set to ready and spawn player
            Debug.Log("[SteamNetworkManager] Connected to UNET server.");
            SteamClient sClient = m_networkClient as SteamClient;
            /// Remove the connection handler
            sClient.UnregisterHandler(MsgType.Connect);

            RegisterNetworkPrefabs();

            NetworkConnection _conn = m_networkClient.connection;
            if (_conn != null)
            {
                ClientScene.Ready(_conn);
                Debug.Log("[SteamNetworkManager] Requesting spawn");
                if (!Server.IsHostingServer)
                {
                    ///Set lobby not ready
                    SteamHelper.SendMessage(m_lobbyID, MessageType.PlayerJoined,System.BitConverter.GetBytes(true));
                }

                //m_networkClient.Send(NetworkMessages.SpawnRequestMsg, new StringMessage(SteamUser.GetSteamID().m_SteamID.ToString()));
            }
        }

        /// <summary>
        /// Called from LobbyManager
        /// if the nextState is: Closed
        /// Close the lobby on SteamAPI
        /// </summary>
        /// <param name="_nextState"></param>
        public void ChangeLobbyState(LobbyState _nextState)
        {
            if (_nextState == LobbyState.Closed)
            {
                Debug.Log("[SteamNetworkManager] Lobby Closed");
                SteamMatchmaking.SetLobbyJoinable(m_lobbyID, false);
                EventManager.TriggerEvent(EventsID.GAMESTARTED);
            }
            else
            {
                SteamMatchmaking.SetLobbyJoinable(m_lobbyID, true);
            }
        }

    }
    #endregion
}