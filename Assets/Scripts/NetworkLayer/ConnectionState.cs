﻿namespace MatchZilla
{
    /// <summary>
    /// All possible state of the session
    /// </summary>
    public enum ConnectionState
    {
        UNDEFINED,
        CONNECTING,
        CANCELLED,
        CONNECTED,
        FAILED,
        DISCONNECTING,
        DISCONNECTED
    }
}