﻿using UnityEngine;
using UnityEngine.Networking;
namespace MatchZilla
{
    public class SteamClient : NetworkClient
    {
        public SteamNetworkConnection steamConnection {
            get {
                return connection as SteamNetworkConnection;
            }
        }

        public string status { get { return m_AsyncConnect.ToString(); } }

        /// <summary>
        /// This function is used by client to connect at server
        /// </summary>
        public void Connect()
        {
            Debug.Log("[SteamClient] Connecting");
            // Connect to localhost and trick UNET by setting ConnectState state to "Connected", which triggers some initialization and allows data to pass through TransportSend
            Connect("localhost", 0);
            m_AsyncConnect = ConnectState.Connected;

            // manually init connection
            connection.ForceInitialize();
            // send Connected message
            connection.InvokeHandlerNoData(MsgType.Connect);
        }

        public SteamClient(NetworkConnection conn) : base(conn)
        {
        }

        public override void Disconnect()
        {
            m_AsyncConnect = ConnectState.Disconnected;

            if (m_Connection != null & m_Connection.isConnected)
            {
                steamConnection.InvokeHandlerNoData(MsgType.Disconnect);
                steamConnection.CloseP2PSession();
                m_Connection.hostId = -1;
                m_Connection.Disconnect();
                m_Connection.Dispose();
                m_Connection = null;
            }

        }
    }
}