﻿using UnityEngine;
using System.Collections.Generic;

namespace MatchZilla
{
    public class RaceManager : MonoBehaviour
    {
        [SerializeField]
        private List<Transform> m_players;
        [SerializeField]
        private Transform[] m_raceGrid;

        //private void OnEnable()
        //{
        //    EventManager.StartListening(EventsID.RACESTARTED, SetPlayer);
        //}

        //private void OnDisable()
        //{
        //    EventManager.StopListening(EventsID.RACESTARTED, SetPlayer);
        //}


        /// To change with a system to check all player load the scene
        private void Start()
        {
            if(SteamNetworkManager.Instance.Server.IsHostingServer)
                SetPlayer();
        }

        private void SetPlayer()
        {
            for (int i = 0; i < SteamNetworkManager.Instance.Server.Connections.Count; i++)
            {
                Transform m_nextPosition = m_raceGrid[i];
                SteamNetworkManager.Instance.SpawnPlayer(SteamNetworkManager.Instance.Server.Connections[i], m_nextPosition);
            }
        }
    }
}