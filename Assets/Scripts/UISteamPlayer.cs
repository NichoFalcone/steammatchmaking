﻿using UnityEngine;
using UnityEngine.UI;

namespace MatchZilla
{
    public class UISteamPlayer : MonoBehaviour
    {
        /// <summary>
        /// use this flag to define if can interact with this image
        /// use it to difference on lobby or friends field
        /// </summary>
        [SerializeField]
        private bool m_bCanInteract         = false;
        /// <summary>
        /// Refernce to player info
        /// </summary>
        private SteamPlayerInfo m_playerInfo;
        [SerializeField]
        private Text m_steamName            = null;
        [SerializeField]
        private RawImage m_steamImage       = null;

        public SteamPlayerInfo PlayerInfo 
            {
            get => m_playerInfo;
            set {
                m_playerInfo = value;
                SetUpInferface();
            }
        }

        private void SetUpInferface()
        {
            m_steamImage.texture = m_playerInfo.SteamImage;
            m_steamName.text = m_playerInfo.SteamName;
        }

        public void OnInteract()
        {
            if (!SteamNetworkManager.Instance.IsConnectedToUNETServer())
            {
                Debug.LogError("Invite failed, you need to enter in a lobby to invite");
                return;
            }
            SteamHelper.SendInvite(SteamNetworkManager.Instance.LobbyID, m_playerInfo.SteamID);
        }
    }
}