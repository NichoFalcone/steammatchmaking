﻿using UnityEngine;

public enum GameState
{
    Nill,
    GameStart,
    GameEnd,
}
namespace MatchZilla
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField]
        private GameState m_gameState = GameState.Nill;
        private SteamNetworkManager m_networkManager;
        [SerializeField]
        private SceneData m_lobbyScene;

        protected override void Awake()
        {
            base.Awake();
            m_networkManager = FindObjectOfType<SteamNetworkManager>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F10))
                OnGameEnd();
        }

        #region GameStateChange
        public void OnGameStateChange(GameState _gameState)
        {
            m_gameState = _gameState;
            switch (m_gameState)
            {
                case GameState.GameStart:
                    OnGameStart();
                    break;
                case GameState.GameEnd:
                    OnGameEnd();
                    break;
                case GameState.Nill:
                    break;
                default:
                    break;
            }
        }

        public void OnGameEnd()
        {
            SceneController.Instance.LoadScene(m_lobbyScene);
            m_networkManager.Disconnect();
        }

        private void OnGameStart()
        {
            Debug.Log("[GameManager] Game started");
            if (m_networkManager.Server.IsHostingServer)
            {
                EventManager.TriggerEvent(EventsID.RACESTARTED);
            }
        }
        #endregion
    }
}
