﻿using UnityEngine;

public class QuitButton : MonoBehaviour
{
    public void OnClick()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
}
