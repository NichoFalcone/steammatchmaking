﻿using System;
using System.Collections.Generic;

public static class FuzzyMatcher
{
    /// <summary>
    /// Does a fuzzy search for a pattern within a string.
    /// </summary>
    /// <param name="_stringToSearch">The string to search for the pattern in.</param>
    /// <param name="_pattern">The pattern to search for in the string.</param>
    /// <returns>true if each character in pattern is found sequentially within stringToSearch; otherwise, false.</returns>
    public static bool FuzzyMatch(string _stringToSearch, string _pattern)
    {
        int patternIdx          = 0;
        int strIdx              = 0;
        int patternLength       = _pattern.Length;
        int strLength           = _stringToSearch.Length;

        while (patternIdx != patternLength && strIdx != strLength)
        {
            if (char.ToLower(_pattern[patternIdx]) == char.ToLower(_stringToSearch[strIdx]))
                ++patternIdx;
            ++strIdx;
        }

        return patternLength != 0 && strLength != 0 && patternIdx == patternLength;
    }
}