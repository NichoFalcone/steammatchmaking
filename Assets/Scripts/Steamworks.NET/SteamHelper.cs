﻿using Steamworks;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace MatchZilla
{
    public static class SteamHelper
    {
        #region Private - Field
        /// <summary>
        /// Used to get the lobby data
        /// </summary>
        private static int m_dataBufferSize = 512;
        ///Check for SteamImage
        private static Dictionary<CSteamID, Texture2D> UserImanageCache = new Dictionary<CSteamID, Texture2D>();
        private static ulong _AvatarTaskCount = 0;
        private static Dictionary<ulong, Callback<AvatarImageLoaded_t>> _AvatarTaskList = new Dictionary<ulong, Callback<AvatarImageLoaded_t>>();
        #endregion

        #region Useflue Function
        public static bool GetLobbyData(CSteamID _lobbyID, out string[] _data)
        {
            string[] _roomInfo = new string[] { " ", " " };
            if (!SteamManager.Initialized)
            {
                _data = null;
                return false;
            }

            if (SteamMatchmaking.GetLobbyDataByIndex(_lobbyID, 0, out _roomInfo[0], m_dataBufferSize, out _roomInfo[1], m_dataBufferSize))
            {
                _data = _roomInfo;
                return true;
            }
            _data = null;
            return false;
        }

        /// <summary>
        /// This function return the user name of a specific userID
        /// </summary>
        /// <param name="_userID"></param>
        /// <returns></returns>
        public static string GetUserName(CSteamID _userID)
        {
            if (!SteamManager.Initialized)
                return null;

            return SteamFriends.GetFriendPersonaName(_userID);
        }
        /// <summary>
        /// Return the user name of the users on a lobby
        /// </summary>
        /// <param name="_lobbyID">lobby id</param>
        /// <returns></returns>
        public static string[] GetUsersName(CSteamID _lobbyID)
        {
            return null;
        }

        /// <summary>
        /// Return the user name of the users on a lobby
        /// </summary>
        /// <param name="_lobbyID">lobby id</param>
        /// <returns></returns>
        public static string[] GetUsersNameFromList(List<CSteamID> _lobbyID)
        {
            List<string> name = new List<string>();
            foreach (CSteamID iD in _lobbyID)
            {
                name.Add(GetUserName(iD));
            }
            return name.ToArray();
        }
        /// <summary>
        /// Get the user image of a user
        /// with a action
        /// </summary>
        /// <param name="steamID">user id</param>
        /// <param name="callback">action to return rhe texture image</param>
        public static void GetUserAvatar(CSteamID steamID, System.Action<Texture2D> callback)
        {
            if (UserImanageCache.ContainsKey(steamID))
            {
                callback(UserImanageCache[steamID]);
                return;
            }

            int userAvatar = SteamFriends.GetLargeFriendAvatar(steamID);
            uint imageWidth;
            uint imageHeight;

            bool restartAvatarLoad = true;
            bool success = SteamUtils.GetImageSize(userAvatar, out imageWidth, out imageHeight);

            if (success && imageWidth > 0 && imageHeight > 0)
            {
                byte[] data = new byte[imageWidth * imageHeight * 4];
                var returnTex = new Texture2D((int)imageWidth, (int)imageHeight, TextureFormat.RGBA32, false, false);

                success = SteamUtils.GetImageRGBA(userAvatar, data, (int)(imageWidth * imageHeight * 4));
                if (success)
                {
                    restartAvatarLoad = false;
                    returnTex.LoadRawTextureData(data);
                    returnTex.Apply();

                    //NOTE: texture loads upside down, so we flip it to normal...
                    var result = FlipTexture(returnTex);

                    UserImanageCache.Add(steamID, result);

                    callback(result);

                    Texture2D.DestroyImmediate(returnTex);
                }
            }

            if (restartAvatarLoad)
            {
                ulong key = _AvatarTaskCount;
                _AvatarTaskCount++;

                var task = new Callback<AvatarImageLoaded_t>(delegate (AvatarImageLoaded_t param)
                {
                    GetUserAvatar(steamID, callback);
                    _AvatarTaskList.Remove(key);
                });

                _AvatarTaskList.Add(key, task);
            }
        }

        /// <summary>
        /// Flip the texture
        /// </summary>
        /// <param name="original"></param>
        /// <returns></returns>
        public static Texture2D FlipTexture(Texture2D original)
        {
            Texture2D flipped = new Texture2D(original.width, original.height);

            int xN = original.width;
            int yN = original.height;

            for (int i = 0; i < xN; i++)
            {
                for (int j = 0; j < yN; j++)
                {
                    flipped.SetPixel(i, yN - j - 1, original.GetPixel(i, j));
                }
            }

            flipped.Apply();

            return flipped;
        }
        #endregion

        /// <summary>
        /// Use this function to send an invite to a designed user
        /// </summary>
        /// <param name="_lobbyId">lobby ID</param>
        /// <param name="_userId">User ID</param>
        /// <returns>if </returns>
        public static bool SendInvite(CSteamID _lobbyId, CSteamID _userId)
        {
            if (SteamMatchmaking.InviteUserToLobby(_lobbyId, _userId))
            {
                Debug.Log("Sending invite to: " + GetUserName(_userId));
                return true;
            }
            Debug.LogError("Sending invite fail");
            return false;
        }

        #region Read and Send message
        /// <summary>
        /// Cached data to read message
        /// </summary>
        static CSteamID _user;
        static EChatEntryType _type;

        /// <summary>
        /// Sending a message to the lobby with a flag to identifie it
        /// </summary>
        /// <param name="_steamID">lobby target</param>
        /// <param name="_messageType">flag</param>
        /// <param name="_data">data body</param>
        /// <returns></returns>
        public static bool SendMessage(CSteamID _steamID, MessageType _messageType, byte[] _data)
        {
            byte _type = (byte)_messageType;
            byte[] _mergedData = AddByteToArray(_data, _type);
            return SteamMatchmaking.SendLobbyChatMsg(_steamID, _mergedData, _mergedData.Length);
        }

        /// <summary>
        /// Read the received message and read the resulting flag and extrapolate the received data
        /// </summary>
        /// <param name="_lobbyID">lobby id</param>
        /// <param name="_chatID">chat id </param>
        /// <param name="_dataResoult">data resoult</param>
        /// <returns></returns>
        public static MessageType ReadMessage(CSteamID _lobbyID, int _chatID, out byte[] _dataResoult)
        {
            byte[] _data = new byte[128];
            SteamMatchmaking.GetLobbyChatEntry(_lobbyID, _chatID, out _user, _data, _data.Length, out _type);
            _dataResoult = _data;
            return ReadHead(_data[0]);
        }


        /// <summary>
        /// Return a enum type from a byte
        /// </summary>
        /// <param name="_messageType"></param>
        /// <returns></returns>
        public static MessageType ReadHead(byte _messageType)
        {
            MessageType type = (MessageType)_messageType;
            Debug.Log(type);
            return type;
        }

        /// <summary>
        /// Add a byte on the head of an byte array
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="newByte"></param>
        /// <returns></returns>
        public static byte[] AddByteToArray(byte[] bArray, byte newByte)
        {
            byte[] newArray = new byte[bArray.Length + 1];
            bArray.CopyTo(newArray, 1);
            newArray[0] = newByte;
            return newArray;
        }
        #region ConvertData
        /// <summary>
        /// Convert an array of byte to a Int32
        /// </summary>
        /// <param name="_rowData"></param>
        /// <param name="_startIndex"></param>
        /// <returns></returns>
        public static int ReadInt(byte[] _rowData, int _startIndex = 1)
        {
            return BitConverter.ToInt32(_rowData, _startIndex);
        }

        /// <summary>
        /// Convert an array of byte to a bool
        /// </summary>
        /// <param name="_rowData"></param>
        /// <param name="_startIndex"></param>
        /// <returns></returns>
        public static bool ReadBool(byte[] _rowData, int _startIndex = 1)
        {
            return BitConverter.ToBoolean(_rowData, _startIndex);
        }

        /// <summary>
        /// Convert an array of byte to a string
        /// </summary>
        /// <param name="_rowData"></param>
        /// <param name="_startIndex"></param>
        /// <returns></returns>
        public static string ReadString(byte[] _rowData, int _startIndex = 1)
        {
            return Encoding.ASCII.GetString(_rowData, _startIndex, _rowData.Length - _startIndex);
        }
        #endregion
        #endregion
    }
}