﻿using Steamworks;
using UnityEngine;
using UnityEngine.UI;

namespace MatchZilla
{
    public class LobbyPlayer : MonoBehaviour
    {
        [SerializeField]
        private CSteamID m_steamID;

        [SerializeField]
        private Text m_name;
        [SerializeField]
        private Button m_readyButton;
        [SerializeField]
        private bool m_ready;
        [SerializeField]
        private Text m_state;

        public bool Ready { get => m_ready; set { m_ready = value; OnStateChange();} }

        public CSteamID SteamID { get => m_steamID; set { m_steamID = value; Init(); } }

        private void Init()
        {
            m_ready = false;
            m_name.text = SteamHelper.GetUserName(m_steamID);
            m_state.text = "Not Ready";
        }

        public void OnStateChange()
        {
            m_state.text = m_ready ? "Ready" : "Not Ready";


            Debug.Log("Reciving Message " + m_ready);
        }

        public void ChangeState()
        {
            m_ready = !m_ready;
            Debug.Log("Sending MEssage "+ m_ready);
            SteamHelper.SendMessage(SteamNetworkManager.Instance.LobbyID, MessageType.PlayreReady, System.BitConverter.GetBytes(m_ready));
        }
    }
}