﻿using Steamworks;
using UnityEngine;

namespace MatchZilla {
    public class InviteButton : MonoBehaviour
    {
        public void OnInteract()
        {
            if (SteamNetworkManager.Instance.IsConnectedToUNETServer())
            {
                Debug.Log("Open overlay");
                SteamFriends.ActivateGameOverlayInviteDialog(SteamNetworkManager.Instance.LobbyID);
            }
        }
    }
}