﻿
// LAST ID = ENDDIALOGUE = 6

public enum EventsID
{
    #region Game Flow
    GAMESTARTED = 1,
    GAMESTATECHANGED = 2,
    PLAYERDEAD = 3,
    GAMEOVER = 4,
    GAMEWIN = 5,
    #endregion

    #region Net
    JOININGROOM = 100,
    RACESTARTED = 101,
    #endregion
}
