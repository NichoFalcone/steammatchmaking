﻿using Steamworks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MatchZilla
{
    internal class UILobbyPanelInfo : MonoBehaviour
    {
        [SerializeField]
        private Text m_lobbyName = null;
        [SerializeField]
        private Text m_gameMode = null;
        [SerializeField]
        private Text m_numberOfPlayer = null;
        [SerializeField]
        private CanvasGroup m_canvasGroup = null;
        [SerializeField]
        private Transform m_playerNameHolder = null;
        [SerializeField]
        private LobbyPlayer m_playerLobby = null;

        private List<LobbyPlayer> m_players = new List<LobbyPlayer>();

        public void ChangePlayerState(CSteamID _target, bool _nextState)
        {
            LobbyPlayer _player = m_players.Find((x) => x.SteamID == _target);
            _player.Ready = _nextState;
        }

        internal void UpdateGenericInfo(string _lobbyName, string _lobbyGameMode, string[] _roomPlayerNum, List<CSteamID> _steamIDs)
        {
            m_lobbyName.text = _lobbyName;
            m_gameMode.text = _lobbyGameMode;
            m_numberOfPlayer.text = _roomPlayerNum[0] + "/" + _roomPlayerNum[1];

            foreach (Transform transform in m_playerNameHolder)
            {
                Destroy(transform.gameObject);
            }

            foreach (CSteamID steamID in _steamIDs)
            {
                if (!m_players.Find((x) => x.SteamID == steamID))
                {
                    LobbyPlayer _player = Instantiate(m_playerLobby, m_playerNameHolder);
                    _player.SteamID = steamID;
                    m_players.Add(_player);
                }
            }
        }
    }
}