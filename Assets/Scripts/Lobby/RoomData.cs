﻿namespace MatchZilla
{
    /// <summary>
    /// Base class containing the informatio of the room
    /// </summary>
    public abstract class RoomData
    {
        protected LobbyData m_lobbyData;
        public LobbyData LobbyData  => m_lobbyData;

        protected RoomData(LobbyData _lobbyData)
        {
            this.m_lobbyData        = _lobbyData;
        }
    }

}