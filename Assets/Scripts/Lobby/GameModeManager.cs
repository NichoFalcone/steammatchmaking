﻿using System.Collections.Generic;
using UnityEngine;

public class GameModeManager : Singleton<GameModeManager>
{
    [SerializeField]
    private GameMode m_defaultGameMode = null;
    [SerializeField]
    private List<GameMode> m_gameModes = new List<GameMode>();


    public List<GameMode> GameModes {
        get { return m_gameModes; }
    }

    /// <summary>
    /// Find a GameMode using a FuzzyMatch
    /// </summary>
    /// <param name="_gameModeName"></param>
    /// <param name="_gameMode"></param>
    /// <returns></returns>
    public bool FindGameMode(string _gameModeName, out GameMode _gameMode)
    {
        if (m_gameModes == null || _gameModeName == null || _gameModeName == " ")
        {
            Debug.LogWarning("[GameModeManager] Something gose wrong searching the gamemode");
            _gameMode = m_defaultGameMode;
            return false;
        }

        if (_gameMode = m_gameModes.Find((x) => FuzzyMatcher.FuzzyMatch(x.GameModeName, _gameModeName)))
        {
            return true;
        }
        else
        {
            _gameMode = m_defaultGameMode;
            return false;
        }
    }

    public bool FindGameMode(int _gameModeIndex, out GameMode _gameMode)
    {
        if (m_gameModes == null || _gameModeIndex > m_gameModes.Count-1)
        {
            Debug.LogWarning("[GameModeManager] Something gose wrong searching the gamemode");
            _gameMode = m_defaultGameMode;
            return false;
        }
        _gameMode = m_gameModes[_gameModeIndex];
        return true;
    }

}
