﻿using Steamworks;
using System.Collections.Generic;

namespace MatchZilla
{
    public class SteamRoomData : RoomData
    {
        private List<CSteamID>  m_users     = new List<CSteamID>();
        private CSteamID        m_owner;
        private CSteamID        m_lobbyID;

        public CSteamID Owner       => m_owner;
        public CSteamID LobbyID     => m_lobbyID;
        public List<CSteamID> Users => m_users;

        public SteamRoomData( LobbyData _lobbyData) : base( _lobbyData)
        { 
            m_lobbyData = _lobbyData;
        }

        public SteamRoomData(LobbyData _lobbyData, List<CSteamID> _users, CSteamID _owner, CSteamID _lobbyReference) : base(_lobbyData)
        {
            m_lobbyData = _lobbyData;
            m_users = _users;
            m_owner = _owner;
            m_lobbyID = _lobbyReference;
        }
    }
}
