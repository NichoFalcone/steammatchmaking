﻿using MatchZilla;
using System.Collections;
using UnityEngine;

public class LobbyManager : MonoBehaviour
{
    /// <summary>
    /// Reference to the NetworkManager
    /// </summary>
    private SteamNetworkManager m_networkManager = null;
    /// <summary>
    /// Reference of the lobby info and steaminfo
    /// </summary>
    [SerializeField]
    private SteamRoomData m_steamRoomData       = null;
    /// <summary>
    /// Current time
    /// </summary>
    private float m_time = 0;
    [SerializeField]
    private bool m_playersReady = true;
    private IEnumerator m_startingGame;

    public delegate void SetTimerHandler(int _time);
    public static event SetTimerHandler OnSetTimer;

    public SteamRoomData RoomData {
        get => m_steamRoomData;
    }

    #region Unity-Method
    private void Awake()
    {
        ///Get NetworkManager
        m_networkManager = FindObjectOfType<SteamNetworkManager>();
    }

    private void OnEnable()
    {
        ///Event Sub
        m_networkManager.OnUserChange += UpdateUsers;
        m_networkManager.OnReciveTime += SetNewTime;
        m_networkManager.OnLobbyStateChange += (bool state) =>
        {
            Debug.LogError("Change state");
            m_playersReady = state;
        };
    }

    private void OnDisable()
    {
        ///Events unsub
        m_networkManager.OnUserChange -= UpdateUsers;
        m_networkManager.OnReciveTime -= SetNewTime;
        ///Hold the start game while the player is not ready
        m_networkManager.OnLobbyStateChange -= (bool state) =>
        {
            m_playersReady = state;
        };
    }
    #endregion

    IEnumerator StartingGame()
    {
        while (!m_playersReady)
        {
            yield return null;
            Debug.Log("Waiting for connecion");
        }
        Debug.Log("[LobbyManager] Game Started");
        SceneController.Instance.LoadSceneAsync(m_steamRoomData.LobbyData.GameMode.GameScene[Random.Range(0, m_steamRoomData.LobbyData.GameMode.GameScene.Length)],                                           
                                                ()=> GameManager.Instance.OnGameStateChange(GameState.GameStart));
    }

    /// <summary>
    /// Called to notified the change of the lobby
    /// </summary>
    /// <param name="_nextState"></param>
    private void ChangeState(LobbyState _nextState)
    {
        m_networkManager.ChangeLobbyState(_nextState);
        m_steamRoomData.LobbyData.LobbyState = _nextState;
    }

    private void UpdateUsers(SteamRoomData _data)
    {
        ///set the new RoomData
        m_steamRoomData = _data;

        if (_data.Users.Count == _data.LobbyData.GameMode.MinLobbyPlayer)
            StartTimer();
        ///Check if the user count has reach the maxlobbyPlayer
        ///close the lobby and start the game
        if (_data.Users.Count == _data.LobbyData.GameMode.MaxLobbyPlayer)
        {
            if(m_startingGame == null)
            {
                m_startingGame = StartingGame();
                StartCoroutine(m_startingGame);
            }
            StopCoroutine(SendCurrentTime());
            ChangeState(LobbyState.Closed);
        }
        if (SteamNetworkManager.Instance.ConnectionState != ConnectionState.DISCONNECTED)
        {
            ///Update the UI
            EventManager.TriggerEvent<SteamRoomData>(EventsID.JOININGROOM, _data);
        }
    }

    private void StartTimer()
    {
        if (!SteamNetworkManager.Instance.Server.IsHostingServer)
            return;
        Debug.Log("Starting Timer");
        m_time = m_steamRoomData.LobbyData.GameMode.MaxWaitingTime;
        StartCoroutine(SendCurrentTime());
    }

    private void SetNewTime(int _currentTime)
    {
        m_time = _currentTime;
        OnSetTimer((int)m_time);
        if (m_time <= 0.01f)
        {
            if (m_startingGame == null)
            {
                m_startingGame = StartingGame();
                StartCoroutine(m_startingGame);
            }
        }
    }

    public void SearchingGame()
    {
        m_networkManager.FindMatch();
    }

    /// <summary>
    /// Dev method to instantly start the game
    /// </summary>
    public void SetTimeAtZero()
    {
        if (SteamNetworkManager.Instance.Server.IsHostingServer)
        {
            StopCoroutine(SendCurrentTime());
            SetNewTime(-1);
            SteamHelper.SendMessage(m_steamRoomData.LobbyID, MessageType.Timer, System.BitConverter.GetBytes(m_time));
        }
    }

    IEnumerator SendCurrentTime()
    {
        ///Do it on this way to reduce the sending package
        while (m_time > 0)
        {
            m_time--;
            Debug.Log(m_time);
            SteamHelper.SendMessage(m_steamRoomData.LobbyID, MessageType.Timer, System.BitConverter.GetBytes((int)m_time));
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForEndOfFrame();
    }
}
