﻿using UnityEngine;

namespace MatchZilla
{
    [System.Serializable]
    public class LobbyData : ILobbyData
    {
        #region Variable
        /// <summary>
        /// Reference of the current state of the lobby
        /// </summary>
        [SerializeField]
        private LobbyState m_lobbyState = LobbyState.Disconnect;
        /// <summary>
        /// lobby game name
        /// </summary>
        [SerializeField]
        private string m_roomName       = "spacewave-unet-p2p-example";
        /// <summary>
        /// Reference of the current GameMode
        /// </summary>
        [SerializeField]
        private GameMode m_gameMode     = null;
        /// <summary>
        /// Give the LobbyName
        /// </summary>
        public string RoomName {
            get { return m_roomName; } set { m_roomName = value; }
        }
        /// <summary>
        /// Reference to the LobbyState
        /// </summary>
        public LobbyState LobbyState {
            get => m_lobbyState;
            set => m_lobbyState = value;
        }
        /// <summary>
        /// Give the contetent of the gamemode
        /// </summary>
        public GameMode GameMode => m_gameMode;
        #endregion

        public LobbyData(LobbyState m_lobbyState, string m_gameID, GameMode m_gameMode, LobbyState lobbyState)
        {
            this.m_lobbyState = m_lobbyState;
            this.m_roomName = m_gameID;
            this.m_gameMode = m_gameMode;
            LobbyState = lobbyState;
        }

        public void OnGameModeChange(int _gameModeIndex)
        {
            GameModeManager.Instance.FindGameMode(_gameModeIndex, out m_gameMode);
        }

        public void OnGameModeChange(string _nextGameMode)
        {
            GameModeManager.Instance.FindGameMode(_nextGameMode, out m_gameMode);
        }

        public void SetRoomName(string _name)
        {
            RoomName = _name;
        }

    }
}