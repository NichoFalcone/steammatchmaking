﻿using UnityEngine;

[CreateAssetMenu(menuName = "GameMode/NewGameMode")]
public class GameMode : ScriptableObject
{
    [SerializeField]
    private string m_gameModeName   = "New Game Mode";
    [SerializeField]
    private SceneData[] m_gameScene = new SceneData [0];
    [SerializeField]
    private int m_minLobbyPlayer    = 2;
    [SerializeField]
    private int m_maxLobbyPlayer    = 4;
    [SerializeField]
    private int m_maxWaitingTime    = 120;

    public string GameModeName {
        get => m_gameModeName;
    }

    public SceneData[] GameScene {
        get => m_gameScene;
    }

    public int MinLobbyPlayer {
        get => m_minLobbyPlayer;
    }

    public int MaxLobbyPlayer {
        get => m_maxLobbyPlayer;
    }
    public int MaxWaitingTime {
        get => m_maxWaitingTime;
    }

}
