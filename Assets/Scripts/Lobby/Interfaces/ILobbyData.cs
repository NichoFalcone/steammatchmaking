﻿namespace MatchZilla
{
    /// <summary>
    /// All possible state of the lobby
    /// </summary>
    public enum LobbyState : int
    {
        Disconnect = -1,
        Waiting,
        StartingMatch,
        Closed
    }

    public interface ILobbyData
    {
        /// <summary>
        /// The current game mode loaded
        /// </summary>
        GameMode GameMode {
            get;
        }
        /// <summary>
        /// The current state of the lobby
        /// </summary>
        LobbyState LobbyState {
            get; set;
        }
        /// <summary>
        /// The RoomName
        /// </summary>
        string RoomName {
            get; set;
        }

        void SetRoomName(string _name);

        /// <summary>
        /// Called to change the game mode by a index
        /// </summary>
        /// <param name="_gameModeIndex"></param>
        void OnGameModeChange(int _gameModeIndex);
        /// <summary>
        /// Called to change the game mode by a name
        /// </summary>
        /// <param name="_nextGameMode"></param>
        void OnGameModeChange(string _nextGameMode);
    }
}