﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using MatchZilla;

public class DropDownHandler : MonoBehaviour
{
    private Dropdown m_dropDown = null;

    private void Awake()
    {
        m_dropDown = GetComponent<Dropdown>();
    }

    private void Start()
    {
        AssigneGameModeValue();
    }

    /// <summary>
    /// This function go to set the value of the drop down with the possible GameMode
    /// taked from GameModeManager
    /// </summary>
    private void AssigneGameModeValue()
    {
        m_dropDown.options = new List<Dropdown.OptionData>();
        foreach (GameMode _gameMode in GameModeManager.Instance.GameModes)
        {
            Dropdown.OptionData _optionData = new Dropdown.OptionData(_gameMode.GameModeName);
            m_dropDown.options.Add(_optionData);
        }
        SteamNetworkManager.Instance.LobbyData.OnGameModeChange(m_dropDown.value);
    }

    /// <summary>
    /// Go to notifie the NetworkManager the change of GameMode
    /// </summary>
    public void GameModeChange()
    {
        SteamNetworkManager.Instance.LobbyData.OnGameModeChange(m_dropDown.value);
    }
}
