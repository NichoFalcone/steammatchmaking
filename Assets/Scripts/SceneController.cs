﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : Singleton<SceneController>
{
    [SerializeField]
    private SceneData m_lobbyScene;

    public void LoadScene(SceneData _data, LoadSceneMode _mode = LoadSceneMode.Single, System.Action action = null)
    {
        SceneManager.LoadScene(_data.SceneName, _mode);
        if (action != null)
            action.Invoke();
    }

    public void LoadSceneAsync(SceneData _data, System.Action action = null)
    {
        this.StartCoroutine(SceneLoading(_data, action));
    }

    IEnumerator SceneLoading(SceneData _data, System.Action action = null)
    {
        AsyncOperation _asyncOperation = SceneManager.LoadSceneAsync(_data.SceneName);

        while (_asyncOperation.isDone && _asyncOperation.progress < .9f)
        {
            yield return null;
        }
        ///Albitrary time to make unload the other
        yield return new WaitForSeconds(0.5f);
        if (action != null)
            action.Invoke();
    }

    public void LoadLobbyScene()
    {
        LoadScene(m_lobbyScene);
    }
}   
