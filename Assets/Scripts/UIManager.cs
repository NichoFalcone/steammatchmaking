﻿using UnityEngine;
using MatchZilla;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Text m_waiting = null;
    [SerializeField]
    private Text m_connectionStatus;
    [SerializeField]
    private Button m_findMatchButton;
    private UILobbyPanelInfo m_lobbyPanel = null;
    private string m_baseMessage = null;

    private void Awake()
    {
        m_lobbyPanel = FindObjectsOfTypeAll(typeof(UILobbyPanelInfo))[0] as UILobbyPanelInfo;
    }

    private void Start()
    {
        m_lobbyPanel.gameObject.SetActive(false);
        m_baseMessage = m_waiting.text;
    }

    private void OnEnable()
    {
        LobbyManager.OnSetTimer += SetTimeLable;
        SteamNetworkManager.Instance.OnConnectionStateChange += SetConnectionStatus;
        EventManager.StartListening<SteamRoomData>(EventsID.JOININGROOM, SetUpLobby);
    }
    private void OnDisable()
    {
        LobbyManager.OnSetTimer -= SetTimeLable;
        EventManager.StopListening<SteamRoomData>(EventsID.JOININGROOM, SetUpLobby);
        SteamNetworkManager.Instance.OnConnectionStateChange -= SetConnectionStatus;
    }

    private void SetConnectionStatus(ConnectionState _state)
    {
        if (_state == ConnectionState.UNDEFINED || _state == ConnectionState.FAILED)
        {
            m_findMatchButton.interactable = false;
            m_connectionStatus.color = Color.red;
            m_connectionStatus.text = "No connection with Steam";
        }
        else
        {
            m_findMatchButton.interactable = true;
            m_connectionStatus.color = Color.black;
            m_connectionStatus.text = _state.ToString();
        }
    }

    private void SetTimeLable(int _time)
    {
        m_waiting.text = m_baseMessage + "\n " + _time;
    }

    private void SetUpLobby(SteamRoomData _steamRoomData)
    {
        if (_steamRoomData == null)
            return;
        Debug.Log("[UIMANAGER] setup Lobby");

        ///This function return an array of info with lenght 2
        ///on this case the 0 will the gamemode and the 1 is the lobbyName
        string[] _data;
        if(!SteamHelper.GetLobbyData(_steamRoomData.LobbyID, out _data))
            Debug.LogError("[UIMANAGER] lobby data not found");

        if (!m_waiting.gameObject.activeInHierarchy && _steamRoomData.Users.Count >= 1)
        {
            m_waiting.gameObject.SetActive(true);
        }


        if (_data == null)
        {
            _data = new string[2];
            _data[0] = _steamRoomData.LobbyData.GameMode.GameModeName;
            _data[1] = _steamRoomData.LobbyData.RoomName;
        }

        ///Active the Panel info
        m_lobbyPanel.gameObject.SetActive(true);
        ///Assigne the information
        m_lobbyPanel.UpdateGenericInfo(_data[1], _data[0], new string[] { _steamRoomData.Users.Count.ToString(), _steamRoomData.LobbyData.GameMode.MaxLobbyPlayer.ToString() }, _steamRoomData.Users);
    }
}
