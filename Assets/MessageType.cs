﻿namespace MatchZilla
{
    public enum MessageType : byte
    {
        ///Bool
        PlayerJoined = 1,
        PlayreReady = 2,

        ///Int
        PlayerCount = 100,
        Timer = 101
    }
}