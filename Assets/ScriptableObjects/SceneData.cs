﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "SceneData/NewSceneData")]
public class SceneData : ScriptableObject
{
    [SerializeField]
    private string m_sceneName = "New Scene";

    public string SceneName { get { return m_sceneName; } set { m_sceneName = value; } }
}