﻿using UnityEngine;
using System.Collections;

public abstract class NetworkManager<T> : Singleton<T>
{
    [SerializeField]
    private UNETServer m_server;

    public ConnectionState m_connectionState;

    /// <summary>
    /// Server instance for the p2p connection
    /// </summary>
    public UNETServer Server {
        get { return m_server; }
    }

    /// <summary>
    /// use this function to starting a lobby to join
    /// if the searching has a negative resoult, start a new one lobby
    /// and open a new connection
    /// </summary>
    public abstract void FindMatch();

    /// <summary>
    /// Use this function to join on the server
    /// and start another one and start sincronize the connection
    /// </summary>
    public abstract void Connect();

    /// <summary>
    /// Use this function for kill the server and leave the steam lobby
    /// </summary>
    public abstract void Disconnect();
}
